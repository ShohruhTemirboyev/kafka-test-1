package com.example.kafkatest1.service;

import com.example.kafkatest1.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class PersonService {

    @Qualifier("kafkaTemplate")
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    public PersonService(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public String addPerson(Person person) {
        try {
        CompletableFuture<SendResult<String, Object>> future = kafkaTemplate.send("test-1", person.getName(), person);

        if (future.isCompletedExceptionally()) {
            return "Error: message not sent";
        }
        future.thenAccept(result -> {
            System.out.println("Message sent successfully with offset: " + result.getRecordMetadata().offset());
        }).exceptionally(ex -> {
            System.out.println("Failed to send message: " + ex.getMessage());
            return null;
        });

        return "Message send";
        }
        catch (Exception e){
            return "Error";
        }
    }

}
