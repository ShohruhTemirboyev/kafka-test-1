package com.example.kafkatest1.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic newTopic1(){
        return TopicBuilder.name("test-1")
                .partitions(3)
                .replicas(3)
                .build();
    }
}
