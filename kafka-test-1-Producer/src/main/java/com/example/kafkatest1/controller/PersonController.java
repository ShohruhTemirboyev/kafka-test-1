package com.example.kafkatest1.controller;

import com.example.kafkatest1.Person;
import com.example.kafkatest1.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    PersonService personService;

    @PostMapping("/addPerson")
    public HttpEntity<?> addPerson(@RequestBody Person person){
       String respons= personService.addPerson(person);
       return ResponseEntity.ok(respons);
    }
}
