package com.example.kafkatest1consumer.service;

import com.example.kafkatest1consumer.entity.Person;
import com.example.kafkatest1consumer.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PersonService {
    private Logger logger= LoggerFactory.getLogger(PersonService.class);

    @Autowired
    PersonRepository personRepository;

     public void addPerson(Person person){
         if (personRepository.existsByName(person.getName())){
             logger.warn("This user name alredy exist");
         }
         else {
             logger.info("User saved to database.");
             personRepository.save(person);
         }
     }

}
