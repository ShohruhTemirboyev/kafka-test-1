package com.example.kafkatest1consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTest1ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTest1ConsumerApplication.class, args);
    }

}
