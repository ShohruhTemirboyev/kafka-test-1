package com.example.kafkatest1consumer;

import com.example.kafkatest1consumer.entity.Person;
import com.example.kafkatest1consumer.service.PersonService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaListeners {

        @Autowired
        PersonService personService;
        @KafkaListener(topics = "test-1", groupId = "1",
                containerFactory = "kafkaListenerContainerFactory")
        void listener(ConsumerRecord<String, Person> record){
           personService.addPerson(record.value()   );
    }

}
