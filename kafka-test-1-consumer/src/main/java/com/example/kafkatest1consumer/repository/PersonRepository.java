package com.example.kafkatest1consumer.repository;

import com.example.kafkatest1consumer.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Long> {

    boolean existsByName(String name);
}
